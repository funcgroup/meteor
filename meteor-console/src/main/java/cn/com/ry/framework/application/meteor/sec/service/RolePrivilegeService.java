/****************************************************
 * Description: Service for t_sec_role_privilege
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-18 RY Create File
**************************************************/
package cn.com.ry.framework.application.meteor.sec.service;

import cn.com.ry.framework.application.meteor.sec.entity.RolePrivilegeEntity;
import cn.com.ry.framework.application.meteor.framework.security.dto.TreeNode;
import cn.com.ry.framework.application.meteor.framework.service.XjjService;
import cn.com.ry.framework.application.meteor.framework.web.support.XJJParameter;

import java.util.List;

public interface RolePrivilegeService  extends XjjService<RolePrivilegeEntity> {

	/**
	 * 通过角色id取得权限树
	 * @param roleid
	 * @return
	 */
	public List<TreeNode> listpri(Long roleid);

	public RolePrivilegeEntity getByParam(XJJParameter param);
}
