/****************************************************
 * Description: Service for 私钥信息
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-12-13 reywong Create File
**************************************************/
package cn.com.ry.framework.application.meteor.meteor.rsa.service;
import cn.com.ry.framework.application.meteor.meteor.rsa.entity.RsaEntity;
import cn.com.ry.framework.application.meteor.framework.service.XjjService;
import cn.com.ry.framework.application.meteor.framework.exception.ValidationException;

import java.util.Map;


public interface RsaService  extends XjjService<RsaEntity>{

    /**
    * 导入
    * @param fileId
    */
    Map<String,Object> saveImport(Long fileId) throws ValidationException;
}
