<#--
/****************************************************
 * Description: 菜单的列表页面
 * Copyright:   Copyright (c) 2018
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-12 RY Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<#include "/templates/xjj-list.ftl">
<@navList navs=navArr/>
<div class="row">
    <div class="container-fluid px-0">
        <div class="col px-0" id="arthas-console-card">
            <div id="arthas-console"></div>
        </div>
    </div>
</div>


<script type="text/javascript">

    var arthas_ws = null;
    var arthasInterval = null;

    initArtahs();

    function initArtahs() {
        var arthas_ip = '${meteor_param.arthasIp}';
        var arthas_port = '${meteor_param.arthasPort}';
        var arthas_agentId = '${meteor_param.arthasAgentId}';
        if (arthas_ip == '') {
            XJJ.msger('机器IP不能为空,请在[meteor]-[全局参数设置]中进行配置');
            return;
        } else if (arthas_port == '') {
            XJJ.msger('PORT不能为空');
            return;
        }

        //获取websocket连接
        var path = 'ws://' + arthas_ip + ':' + arthas_port + '/ws';
        if (arthas_agentId != 'null' & arthas_agentId != "" && arthas_agentId != null) {
            path = path + '?method=connectArthas&id=' + arthas_agentId;
        }

        arthas_ws = initArthasConsole("arthas-console", path, "meteor_arthas", true);

        if (arthas_ws != null) {
            arthasInterval = window.setInterval(function () {
                arthas_ws.send(JSON.stringify({action: 'read', data: ""}));
            }, 30000);
        } else {
            clearInterval(arthasInterval);
        }
    }

    function sendCmd(cmd) {
        arthas_ws.send(JSON.stringify({action: 'read', data: cmd + "\r"}));
    }


</script>

